import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  titleSearch:String = "";
  constructor(public app: AppComponent, public router:Router) { }

  ngOnInit() {
  }

  filterByPublisher(publisher){
    this.app.getFilteredFeeds({publisher});
    this.router.navigate(['']);
  }

  filterByTitle(){
    const title = this.titleSearch;
    this.app.getFilteredFeeds({title});
    this.router.navigate(['']);
  }

}
