import { Component, OnInit, Input } from '@angular/core';
import { TruncateTextPipe } from '../../pipes/truncate-text.pipe';
import { Router, NavigationExtras } from "@angular/router";

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
  styleUrls: ['./news-item.component.scss'],
  providers: [TruncateTextPipe]
})
export class NewsItemComponent implements OnInit {
  @Input() title: string;
  @Input() imgSrc: string;
  @Input() description: string;
  @Input() publisher: string;
  @Input() source: string;
  @Input() _id: string;

  constructor(public router: Router) { }

  ngOnInit() {
  }

  detaill() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        publisher: this.publisher,
        source: this.source,
        title: this.title,
        imgSrc: this.imgSrc,
        description: this.description,
        _id: this._id
      }
  };
    this.router.navigate(['detaill'], navigationExtras);
  }
}
