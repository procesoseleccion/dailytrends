import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewsItemComponent } from './components/news-item/news-item.component';
import { LoadingComponent } from './template-components/loading/loading.component';
import { NavbarComponent } from './template-components/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { ListFeedsComponent } from './pages/list-feeds/list-feeds.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { TruncateTextPipe } from './pipes/truncate-text.pipe';
import { DetaillComponent } from './pages/detaill/detaill.component';
import { NewFeedComponent } from './pages/new-feed/new-feed.component';
import { ToastComponent } from './template-components/toast/toast.component';

@NgModule({
  declarations: [
    AppComponent,
    NewsItemComponent,
    LoadingComponent,
    NavbarComponent,
    ListFeedsComponent,
    PageNotFoundComponent,
    TruncateTextPipe,
    DetaillComponent,
    NewFeedComponent,
    ToastComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
