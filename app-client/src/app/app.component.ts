import { Component, OnInit } from '@angular/core';
import { FeedsServiceService } from './services/feeds-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  feeds = [];
  activeFilter:String;
  title = 'app-client';
  loading = false;

  error = null;
  constructor(public feedsService: FeedsServiceService){}

  getAllFeeds(){
    this.loading = true;
    this.feedsService.getAllFeeds().subscribe(
      response => {
        const res:any = response;
        this.feeds = res;
        this.loading = false;
      },
  
      error => {
        this.loading = false;
        this.showError('Error obteniendo feeds', error)
      }
    );
  }

  getFilteredFeeds(filter){
    this.loading = true;
    this.feedsService.getFeedsByFilter(filter).subscribe(
      response => {
        const res:any = response;
        this.feeds = res;
        this.loading = false;
      },
  
      error => {
        this.loading = false;
        this.showError('Error obteniendo feeds', error);
      }
    );
  }

  showError(title, message){
    this.error = {};
    this.error.title = title;
    this.error.message = message.message || message;
    setTimeout(() => this.error = null, 10000);
  }
}
