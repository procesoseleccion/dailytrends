import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-feeds',
  templateUrl: './list-feeds.component.html',
  styleUrls: ['./list-feeds.component.scss']
})

export class ListFeedsComponent implements OnInit {
  constructor(public app: AppComponent, public router:Router) { }

  ngOnInit() {    
    this.app.getAllFeeds();
  }

  goToNewFeed() {
    this.router.navigate(['new-feed']);
  }

}
