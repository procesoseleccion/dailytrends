import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedsServiceService } from '../../services/feeds-service.service';
import { AppComponent } from '../../app.component';
@Component({
  selector: 'app-detaill',
  templateUrl: './detaill.component.html',
  styleUrls: ['./detaill.component.scss'],
  providers: [FeedsServiceService]
})
export class DetaillComponent implements OnInit {
  feed: any;
  bindingfeed;
  constructor(public app: AppComponent, public router: Router, private route: ActivatedRoute, public feedsService: FeedsServiceService) { 
    this.route.queryParams.subscribe(params => {
      this.feed = params;
      this.bindingfeed = Object.assign({}, this.feed);
  });
  }

  ngOnInit() {
  }

  delete(){
    this.app.loading = true;
    this.feedsService.deleteFeed(this.feed._id).subscribe(
      response => {
        this.app.loading = false;
        this.router.navigate([''])
      },
      error => {
        this.app.showError('Error creando la noticia', error);
        this.app.loading = false;
      }
    );
  }

  update(){
    this.app.loading = true;
    this.feedsService.updateFeed(this.bindingfeed).subscribe(
      response => {
        this.app.loading = false;
        this.router.navigate([''])
      },
      error => {
        this.app.showError('Error editando la noticia', error);
        this.app.loading = false;
      }
    );
  }
}
