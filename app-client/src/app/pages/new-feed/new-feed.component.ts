import { Component, OnInit } from '@angular/core';
import { FeedsServiceService } from '../../services/feeds-service.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-new-feed',
  templateUrl: './new-feed.component.html',
  styleUrls: ['./new-feed.component.scss']
})
export class NewFeedComponent implements OnInit {
  bindingfeed= {
    publisher: 'DailyTrends',
    source: `DailyTrends-${new Date()}`,
  };
  constructor(public app:AppComponent, public feedsService: FeedsServiceService, public router:Router) { }

  ngOnInit() {
  }

  create(){
    this.app.loading = true;
    this.feedsService.createFeed(this.bindingfeed).subscribe(
      response => {
        this.app.loading = false;
        console.log('actualizada')
        this.router.navigate([''])
      },
      error => {
        this.app.loading = false;
        console.log(error)
      }
    );
  }

}
