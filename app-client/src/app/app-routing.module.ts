import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListFeedsComponent } from './pages/list-feeds/list-feeds.component'; 
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component'; 
import { DetaillComponent } from './pages/detaill/detaill.component';
import { NewFeedComponent } from './pages/new-feed/new-feed.component';

const routes: Routes = [
  { path: '', component: ListFeedsComponent },
  { path: 'detaill', component: DetaillComponent },
  { path: 'new-feed', component: NewFeedComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
