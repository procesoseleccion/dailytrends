import { TestBed } from '@angular/core/testing';

import { FeedsServiceService } from './feeds-service.service';

describe('FeedsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeedsServiceService = TestBed.get(FeedsServiceService);
    expect(service).toBeTruthy();
  });
});
