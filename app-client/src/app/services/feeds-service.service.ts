import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams  } from '@angular/common/http';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  body: {}
};

@Injectable({
  providedIn: 'root'
})

export class FeedsServiceService {
  constructor(private http: HttpClient) { }

  getAllFeeds() {
    return this.http.get(`${environment.apiUrl}feeds/getFeeds/`, httpOptions);
  }

  getFeedsByFilter(filter) {
    let params = new HttpParams();
    if(filter.title)
      params = params.append('title', filter.title);
    if(filter.body)
      params = params.append('body', filter.body);
    if(filter.source)
      params = params.append('source', filter.source);
    if(filter.publisher)
      params = params.append('publisher', filter.publisher);
    if(filter.image)
      params = params.append('image', filter.image);

    return this.http.get(`${environment.apiUrl}feeds/getFeeds/`, {params});
  }

  deleteFeed(id) {
    httpOptions.body = { _id: id }
    return this.http.delete(`${environment.apiUrl}feeds/deleteFeed/`, httpOptions);
  }

  createFeed(feed) {
    const body = {
      title: feed.title,
      body: feed.description,
      source: feed.source,
      publisher: feed.publisher,
      image: feed.imgSrc
    };

    return this.http.post(`${environment.apiUrl}feeds/createFeed/`, body);
  }

  updateFeed(feed) {
    const body = {
      title: feed.title,
      body: feed.description,
      source: feed.source,
      publisher: feed.publisher,
      image: feed.imgSrc,
      _id: feed._id
    };

    return this.http.put(`${environment.apiUrl}feeds/updateFeed/`, body);
  }
}
