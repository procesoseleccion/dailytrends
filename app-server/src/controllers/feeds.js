'use strict'

const _ = require('lodash');
const feedsBll = require('../bll/feeds');

const validateFeed = (feed) => {
    if (!feed.title)
        return {error: 'Empty title field'};
    
    if (!feed.body)
        return {error: 'Empty body field'};

    if (!feed.image)
        return {error: 'Empty image field'};

    if (!feed.source)
        return {error: 'Empty source field'};

    if (!feed.publisher)
        return {error: 'Empty publisher field'};
};

const createFeed = async (req, res) => {
    if (!req.body || _.isEmpty(req.body)){
        res.status(500).send({error: 'Empty body'});
        return;
    }

    const invalidFeed = validateFeed(req.body);
    if(invalidFeed){
        res.status(500).send(invalidFeed);
        return;
    }

    try {
        const feedCreated = await feedsBll.createFeed(req.body);
        res.status(200).send({'feedCreated': feedCreated.title});
    } catch (error) {
        res.status(500).send(error.toString());
    }
};

const getFeeds = async (req, res) => {
    const feeds = await feedsBll.getFeeds(req.query);
    res.status(200).send(feeds);
}

const updateFeed = async (req, res) => {
    if (!req.body || _.isEmpty(req.body)){
        res.status(500).send({error: 'Empty body'});
        return;
    }

    const invalidFeed = validateFeed(req.body);
    if(invalidFeed){
        res.status(500).send(invalidFeed);
        return;
    }

    try {
        const feeds = await feedsBll.updateFeed(req.body);
        res.status(200).send(feeds);
    } catch (error) {
        res.status(500).send(error.toString());
    }
}

const deleteFeed = async (req, res) => {
    if (!req.body || _.isEmpty(req.body)){
        res.status(500).send({error: 'Empty body'});
        return;
    }

    if (!req.body._id){
        res.status(500).send({error: 'Id required'});
        return;
    }

    try {
        const deleted = await feedsBll.deleteFeedById(req.body._id);
        res.status(200).send(deleted);
    } catch (error) {
        res.status(500).send(error.toString());
    }
}

module.exports = {
    createFeed,
    getFeeds,
    updateFeed,
    deleteFeed
}