'use strict'

const _ = require('lodash');
const feedRepository = require('../repositories/feeds');

const createFeed = async (feed) => {
    const {source} = feed;
    const exist = await feedRepository.getFeedBySource(source);
    if(exist && exist.length > 0)
        throw Error('Feed exist')
    
    return await feedRepository.createFeed(feed);
};

const createFeedByArray = async (feeds) => {
    for (let index = 0; index < feeds.length; index++) {
        const feed = feeds[index];
        try {
            await createFeed(feed);
        } catch (error) {
            console.warn(`${error.toString()} ==> ${feed.source}`)
        }
    }
};

const getFeeds = async (filter) => {
    if(filter && !_.isEmpty(filter)){
        return await feedRepository.getByFilter(filter);
    } else {
        return await feedRepository.getAll();
    }
};

const deleteFeedById = async (id) => {
    return await feedRepository.deleteFeedById(id);
};

const updateFeed = async (feed) => {
    return await feedRepository.updateFeed(feed);
};

module.exports = {
    createFeed,
    getFeeds,
    deleteFeedById,
    updateFeed,
    createFeedByArray
};