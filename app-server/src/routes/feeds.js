'use strict'

const express = require('express');
const router = express.Router();
const feedController = require('../controllers/feeds')

router.post('/createFeed', feedController.createFeed);
router.delete('/deleteFeed', feedController.deleteFeed);
router.get('/getFeeds', feedController.getFeeds);
router.put('/updateFeed', feedController.updateFeed);

module.exports = router;