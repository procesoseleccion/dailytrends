'use strict'

const express = require('express');
const router = express.Router();

// Import Routes
const feedRoutes = require('./feeds');

router.use('/feeds', feedRoutes);

module.exports = router;