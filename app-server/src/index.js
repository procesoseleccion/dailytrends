'use strict'

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const scraperService = require('./services/scraper');
require('dotenv/config');

const app = express();
const port = process.env.PORT || 3000;

// Midlewares
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.json({limit: '150mb'}));

// Import Routes
const routes = require('./routes/index');
app.use(routes);

mongoose.connect(process.env.DB_STRING_CONNECTION, async () => {
  console.log('Mongodb connected');
  await scraperService.wakeUp();
}).catch(() => {
  console.log('Mongodb can\'t connect to database');
});

app.listen(port, function () {
  console.log(`Node listening on port ${port}`);
});
