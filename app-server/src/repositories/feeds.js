'use strict'

const Feed = require('../models/feed');

const getFeedBySource = async (source) => {
    const query = Feed.find({source});
    return await query.exec();
};

const createFeed = async (feed) => {
    const newFeed = new Feed(feed);
    return await newFeed.save();
};

const getByFilter = async (filter) => {
    const filterQuery = {};

    if(filter.source)
        filterQuery.source = filter.source;
    if(filter.body)
        filterQuery.body = filter.body;
    if(filter.title)
        filterQuery.title =  { "$regex": filter.title, "$options": "i" };
    if(filter.image)
        filterQuery.image = filter.image;
    if(filter.publisher)
        filterQuery.publisher = filter.publisher;

    const query = Feed.find(filterQuery);
    return await query.sort({date: -1}).exec();
};

const getAll = async () => {
    return await Feed.find({}).sort({date: -1}).exec();
};

const deleteFeedById = async (id) => {
    return await Feed.findByIdAndDelete(id).exec();
};

const updateFeed = async (feed) => {
    return await Feed.findByIdAndUpdate(feed._id, {$set: feed}).exec();
};

module.exports = {
    getFeedBySource,
    createFeed,
    getByFilter,
    getAll,
    deleteFeedById,
    updateFeed
}