'use strict'

const puppeteer = require('puppeteer');
const feedsBll = require('../bll/feeds');
const CronJob = require('cron').CronJob;

const getHrefs = async (browser) => {
    const page = await browser.newPage();
    await page.setViewport({ width: 1280, height: 1800 })

    await page.goto('https://elpais.com/', {waitUntil: 'load'});

    let href = await page.$$eval('.articulos > .articulos__interior > .articulo > .articulo__interior > h2.articulo-titulo > a', options => { return options.map(option => option.getAttribute('href')) });
    href = href.slice(0, 10);

    await page.goto('https://www.elmundo.es/', {waitUntil: 'load'});

    let href2 = await page.$$eval('.ue-c-cover-content__link', options => { return options.map(option => option.getAttribute('href')) });
    href2 = href2.slice(0, 10);
    return {'ElPais': href, 'ElMundo': href2};
};

const scrapElPais = async (hrefArray, browser) => {
    const page = await browser.newPage();
    await page.setViewport({ width: 1280, height: 1800 });
    const news = [];
    for (let index = 0; index < hrefArray.length; index++) {
        const href = hrefArray[index];
        await page.goto(href, {waitUntil: 'load'});
        const evaluate = await page.evaluate(() => {
            const title = document.querySelector('#articulo-titulo').innerText;
            const body = document.querySelector('.articulo-cuerpo').innerText;
            const imgs = document.querySelectorAll("div.articulo__contenedor img");
            const image = imgs[0].getAttribute('src');

          return {
            title,
            body,
            image
          }
        });

        evaluate.source = href;
        evaluate.publisher = 'El País';
        news.push(evaluate)
    }

    return news;
};

const scrapElMundo = async (hrefArray, browser) => {
    const page = await browser.newPage();
    await page.setViewport({ width: 1280, height: 1800 });
    const news = [];
    for (let index = 0; index < hrefArray.length; index++) {
        const href = hrefArray[index];
        if(!href.includes('https://www.elmundo.es/')){
            continue;
        }

        await page.goto(href, {waitUntil: 'load'});
        const evaluate = await page.evaluate(() => {
            const title = document.querySelector('.ue-c-article__headline').innerText;
            const body = document.querySelector('.ue-c-article__body').innerText;
            const imgs = document.querySelectorAll('img');

            let imgLinks = [];
            imgs.forEach(element => {
                imgLinks.push(element.getAttribute('src'))
            });

            imgLinks = imgLinks.filter((i) => i.includes('https://e00-elmundo.uecdn.es/'))
            const image = imgLinks[0];

          return {
            title,
            body,
            image,
          }
        });

        evaluate.source = href;
        evaluate.publisher = 'El Mundo';
        news.push(evaluate)
    }

    return news;
};

const saveNews = async (news) => {
    try {
        await feedsBll.createFeedByArray(news);
    } catch (error) {
        console.log(error)        
    }
};

const scrap = async () => {
    try {
        const browser = await puppeteer.launch();
        const href = await getHrefs(browser);
        const elPaisNews = await scrapElPais(href['ElPais'], browser);
        const elMundoNews = await scrapElMundo(href['ElMundo'], browser);
        const news = elPaisNews.concat(elMundoNews);
        await saveNews(news);
        await browser.close();
    } catch (error) {
        console.log(error)
    }
};

const wakeUp = async () => {
    await scrap();
    new CronJob('* 40 * * * *', async () => {
        await scrap();
    }, null, true);
};

module.exports = {wakeUp};