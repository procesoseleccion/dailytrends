const requestApp = require('supertest')
const request = requestApp("http://localhost:3000")

const feedSessionTime = new Date();

const isArray = function (res) {
    return Array.isArray(res.body);
};

describe('TEST FEEDS', function () {
    describe('GET', function () {
        it('Should return json as default data format', function (done) {
            request.get('/feeds/getFeeds')
                .expect('Content-Type', /json/)
                .expect(200, done);
        });
        it('Should return an array', function (done) {
            request.get('/feeds/getFeeds')
                .expect(isArray)
                .expect(200, done);
        });
    });
    describe('POST', function () {
        it('Should create a new feed', function (done) {
            let feed = {
                "title": "Título de prueba",
                "body": "Descripción de prueba",
                "image": "url",
                "source": `TESTMOCHA${feedSessionTime}`,
                "publisher": "provincias"
            };

            request.post('/feeds/createFeed')
                .send(feed)
                .expect(200, done)
        });

        it('Should generate an error because this feed has been created previously', function (done) {
            let feed = {
                "title": "Título de prueba",
                "body": "Descripción de prueba",
                "image": "url",
                "source": `TESTMOCHA${feedSessionTime}`,
                "publisher": "provincias"
            };

            request.post('/feeds/createFeed')
                .send(feed)
                .expect(500, done)
        });
    });
});

