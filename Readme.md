
DailyTrends
===================

Aplicación para gestionar feeds de noticias.

<div style="text-align:center"><img src="https://i.imgur.com/it7zVQB.png" /></div>

Requisitos
===================
- Para que el programa funcione, es necesario tener instalado nodejs y npm:
- Una vez instalado node, mediante npm podremos instalar angular/cli para ejecutar la aplicación en entorno de desarrollo, en producción no es necesario.

Instalar nodejs
-----------------

### Windows
- Descargar instalador .msi de la página de node https://nodejs.org/en/download/

### Instalar en Linux

#### Arch linux

`$ pacman -S nodejs npm`
#### Fedora linux

`$ sudo dnf install -y gcc-c++ make` <br/>
`$ curl -sL https://rpm.nodesource.com/setup_12.x | sudo -E bash -`<br/>
`$ sudo apt-get install nodejs`
#### Sistemas linux basados en Debian

`$ sudo apt-get install curl`<br/>
`$ curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -`<br/>
`$ sudo apt-get install nodejs`


Instalar angular/cli
------------------
`$ npm install -g @angular/cli`

Es posible que en linux te pida permisos de super usuario `$ sudo npm install -g @angular/cli`

Instalar DailyTrends
======================

### Clonar repositorio
`git clone https://gitlab.com/procesoseleccion/dailytrends.git`

### Instalar en Windows
- Ejecutar en cmd o PowerShell

`cd dailyTrends/`<br/>
`windowsInstaller.bat`

### Instalar en linux
- Ejecutar en terminal bash o similar

`cd dailyTrends/`<br/>
`sh linuxInstaller.sh`

Ejecutar DailyTrends
======================

Ejecutar desde la carpeta contenedora de la aplicación usando el terminal.

`npm run dev --prefix ./app-server`

Este comando levantará tanto la aplicación de Angular para el front como la de node para el back, si lo que queremos es levantarlas de forma independiente hay que seguir las siguientes indicaciones:

### Aplicacion angular (front)
Desde la carpeta app-clients dentro de dailyTrends, ejecutamos el comando proporcionado por angular/cli:

`ng serve`

### Aplicacion node (back)
Desde la carpeta app-server dentro de dailyTrends, ejecutamos el comando node que levanta la aplicación desde su fichero de entrada:

`node index.js`

____________________

La aplicación Front se levantará en el puerto 4200 y la api se levantará en el 3000 por defecto

Test API Rest DailyTrends
======================

He añadido algunos test para probar la api, se pueden ejecutar con el siguiente comando desde la carpeta raíz del proyecto:

`npm run test --prefix ./app-server`

![Imgur](https://i.imgur.com/nY5msCL.png)

Arquitectura de la aplicación
======================

<div style="text-align:center"><img src="https://i.imgur.com/VMOvgtW.png" /></div>

El objetivo de esta estructura es que una llamada a la API pase por middlewares/routes -> controllers -> bll -> repositories -> db dejando una responsabilidad a cada capa.

La capa de middlewares en este caso solo parsea el body de las peticiones para poder trabajar con json, pero podríamos añadir más como una autenticación de usuario o un logger que lleve un registro de las llamadas.

Los ficheros de la capa routes se encargan de redireccionar las peticiones a los respectivos controlladores, el sistema de rutas está pensado para que sea modular y por ejemplo poder asignar un middleware a una sola parte de las rutas como todas las rutas que sean solo para usuarios administradores.

Los controllers se van a encargar de controlar los parametros de entrada y de salida de la aplicación y llamarán a su respectiva bll
La capa bll es una capa de "negocio", si tiene que hacer algún cálculo o modificación sobre los datos que va a enviar la API, es esta capa quien se encarga de hacerlos, pero su principal objetivo es que un solo método de la bll llame a todos los repositorios que sean necesarios para obtener los datos.

Por último el repositorio se encarga de hacer todas las llamadas a bases de datos, tener separada esta capa, nos permite por ejemplo tener diferentes bases de datos sin que a la bll o capas superiores le afecten.